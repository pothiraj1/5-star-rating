<?php
/**
 * Plugin Name: 5 Star Rating
 * Plugin URI: #
 * Description: It will help your blog readers to share their reviews in the form of stars. 
 * Version: 1.0
 * Author: Pothiraj
 * Author URI: #
 */
	function starratingRun() {
		wp_register_style( 'font_awesome_stylesheet', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );
		wp_register_style( 'star_rating_css', plugins_url('/css/starcss.css', __FILE__), false, '1.0.0', 'all');
		wp_enqueue_style( 'font_awesome_stylesheet' );
		wp_enqueue_style( 'star_rating_css' );	
		
		wp_register_script('insertrating', plugin_dir_url( __FILE__ ) . 'js/ratingdataInsert.js', array( 'jquery' ));
		wp_localize_script( 'insertrating', 'insertrating_ajax', array( 'ajax_url' => admin_url('admin-ajax.php')) );
		wp_enqueue_script("insertrating");
		
		//add_filter( 'the_content','starRatingDivAdd'  );
		add_shortcode( 'starrating', 'starRatingDivAdd' );			
					
		add_action( 'wp_ajax_star_rating', 'starInsertRating' );
		add_action( 'wp_ajax_nopriv_star_rating', 'starInsertRating'  );		
		
		add_action( 'wp_ajax_initial_data', 'starRatingMainFunction' );
		add_action( 'wp_ajax_nopriv_initial_data', 'starRatingMainFunction'  );
		
		register_activation_hook(__FILE__,'createstarRatingTable');

	}	
	function starRatingDivAdd ( $content ) {
		if(is_single()) {
			global $wpdb;
			$table_name = $wpdb->prefix . "star_rating";
			$current_post_id = get_the_ID();
			$title = get_the_title();
			$post_rating = 0;
			$post_number_of_rating = 0;
			
			$ratings = $wpdb->get_results("SELECT AVG(rating) as r,count(*) as total FROM $table_name where page_id = $current_post_id");
			foreach($ratings as $rating) {
				$post_rating = esc_html($rating->r);
				$post_number_of_rating = ($rating->total);
			}
			return $content .= "<div id='star_rating_div'></div>";
		}
	}
		function starRatingMainFunction ( $content ) {
			$starfive = ""; $starfour = ""; $starthree = ""; $startwo = ""; $starone = ""; $starzero = "";
			global $wpdb;
			$table_name = $wpdb->prefix . "star_rating";
			$url = wp_get_referer();
			$current_post_id = url_to_postid( $url ); 
			//$current_post_id = get_the_ID();
			$post_rating = 0;
			$post_number_of_rating = 0;
			
			$ratings = $wpdb->get_results("SELECT AVG(rating) as r,count(*) as total FROM $table_name where page_id = $current_post_id");
			foreach($ratings as $rating) {
				$post_rating = esc_html($rating->r);
				$post_number_of_rating = esc_html($rating->total);
			}
			
			if($post_rating>4.75) {$fi = 'fa fa-star checked'; $fo = 'fa fa-star checked'; $th = 'fa fa-star checked'; $tw = 'fa fa-star checked'; $on = 'fa fa-star checked'; }
			if($post_rating>4.25 && $post_rating<=4.75) {$fi = 'fa fa-star-half-full checked'; $fo = 'fa fa-star checked'; $th = 'fa fa-star checked'; $tw = 'fa fa-star checked'; $on = 'fa fa-star checked'; }
			if($post_rating>3.75 && $post_rating<=4.25) {$fi = 'fa fa-star'; $fo = 'fa fa-star checked'; $th = 'fa fa-star checked'; $tw = 'fa fa-star checked'; $on = 'fa fa-star checked'; }
			if($post_rating>3.25 && $post_rating<=3.75) {$fi = 'fa fa-star'; $fo = 'fa fa-star-half-full checked'; $th = 'fa fa-star checked'; $tw = 'fa fa-star checked'; $on = 'fa fa-star checked'; }
			if($post_rating>2.75 && $post_rating<=3.25) {$fi = 'fa fa-star'; $fo = 'fa fa-star'; $th = 'fa fa-star checked'; $tw = 'fa fa-star checked'; $on = 'fa fa-star checked'; }
			if($post_rating>2.25 && $post_rating<=2.75) {$fi = 'fa fa-star'; $fo = 'fa fa-star'; $th = 'fa fa-star-half-full checked'; $tw = 'fa fa-star checked'; $on = 'fa fa-star checked'; }
			if($post_rating>1.75 && $post_rating<=2.25) {$fi = 'fa fa-star'; $fo = 'fa fa-star'; $th = 'fa fa-star'; $tw = 'fa fa-star checked'; $on = 'fa fa-star checked'; }
			if($post_rating>1.25 && $post_rating<=1.75) {$fi = 'fa fa-star'; $fo = 'fa fa-star'; $th = 'fa fa-star'; $tw = 'fa fa-star-half-full checked'; $on = 'fa fa-star checked'; }
			if($post_rating>0.75 && $post_rating<=1.25) {$fi = 'fa fa-star'; $fo = 'fa fa-star'; $th = 'fa fa-star'; $tw = 'fa fa-star'; $on = 'fa fa-star checked'; }
			if($post_rating>0.25 && $post_rating<=0.75) {$fi = 'fa fa-star'; $fo = 'fa fa-star'; $th = 'fa fa-star'; $tw = 'fa fa-star'; $on = 'fa fa-star-half-full checked'; }
			if($post_rating>=0 && $post_rating<=0.25) {$fi = 'fa fa-star'; $fo = 'fa fa-star'; $th = 'fa fa-star'; $tw = 'fa fa-star'; $on = 'fa fa-star'; }
			$ratingcount ="<div class='star_rating_count'>
			
			<div class='pull-left'>
				<div class='pull-left' style='width:75px; line-height:1;'>
					<div style='height:20px; margin:5px 0;'>5 Star</div>
				</div>
				<div class='pull-left' style='width:180px;'>
					<div class='progress' style='height:20px; margin:8px 0;'>
					  <div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='5' aria-valuemin='0' aria-valuemax='5' style=''>
						<span class='sr-only'></span>
					  </div>
					</div>
				</div>
				
			</div>
			<div class='pull-left'>
				<div class='pull-left' style='width:75px; line-height:1;'>
					<div style='height:20px; margin:5px 0;'>4 Star</div>
				</div>
				<div class='pull-left' style='width:180px;'>
					<div class='progress' style='height:20px; margin:8px 0;'>
					  <div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='4' aria-valuemin='0' aria-valuemax='5' style=''>
						<span class='sr-only'></span>
					  </div>
					</div>
				</div>
				
			</div>
			<div class='pull-left'>
				<div class='pull-left' style='width:75px; line-height:1;'>
					<div style='height:20px; margin:5px 0;'>3 Star</div>
				</div>
				<div class='pull-left' style='width:180px;'>
					<div class='progress' style='height:20px; margin:8px 0;'>
					  <div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='3' aria-valuemin='0' aria-valuemax='5' style=''>
						<span class='sr-only'></span>
					  </div>
					</div>
				</div>
				
			</div>
			<div class='pull-left'>
				<div class='pull-left' style='width:75px; line-height:1;'>
					<div style='height:20px; margin:5px 0;'>2 Star</div>
				</div>
				<div class='pull-left' style='width:180px;'>
					<div class='progress' style='height:20px; margin:8px 0;'>
					  <div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='2' aria-valuemin='0' aria-valuemax='5' style='width:100%'>
						<span class='sr-only'></span>
					  </div>
					</div>
				</div>
				
			</div>
			<div class='pull-left'>
				<div class='pull-left' style='width:75px; line-height:1;'>
					<div style='height:20px; margin:5px 0;'>1 Star</div>
				</div>
				<div class='pull-left' style='width:180px;'>
					<div class='progress' style='height:20px; margin:8px 0;'>
					  <div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='1' aria-valuemin='0' aria-valuemax='5' style=''>
						<span class='sr-only'></span>
					  </div>
					</div>
				</div>
				
			</div>
		</div>";
			$data = "<p><input type='hidden' id='star-rating-post-id' value='$current_post_id' /><input type='hidden' id='star-rating-plugin-url' value='".plugins_url()."' /><span id='one' class='fa fa-star $on' title='1 Sart'></span> <span id='two' class='fa fa-star $tw' title='2 Sarts'></span> <span id='three' class='fa fa-star $th' title='3 Sarts'></span> <span id='four' class='fa fa-star $fo' title='4 Sarts'></span> <span id='five' class='fa fa-star $fi' title='5 Sarts'></span></p>" . "<div id='star-rating-text'><em>".number_format($post_rating,2)." Ratings By $post_number_of_rating Readers</em></div>";
			
			echo $data;

			echo $ratingcount;
			die();
		
	}
	
	function starInsertRating() {
		global $wpdb;
		$postid = sanitize_text_field($_POST['postid']);
		$rating = sanitize_text_field($_POST['rating']);
		$ipaddress = sanitize_text_field($_SERVER['REMOTE_ADDR']);
		$table_name = $wpdb->prefix . "star_rating";
		$numbers = 0;
		$post_rating = 0;
		$result = $wpdb->insert($table_name, array('rating' => $rating, 'page_id' => $postid, 'ipaddress' => $ipaddress));
		if($result == true) {
			$ratings = $wpdb->get_results("SELECT AVG(rating) r,count(*) t FROM $table_name where page_id = $postid");
			if($ratings == true) {
				foreach($ratings as $rating) {
					$post_rating = esc_html($rating->r);
					$numbers = esc_html($rating->t);
				}
				echo $post_rating.'-'.$numbers;
			} else {
				echo "Error1";
			}
		} else {
			echo "Error2";
		}
		die();
	}
	
	function createstarRatingTable() {
		global $wpdb;
		$table_name = $wpdb->prefix . "star_rating";
		$my_products_db_version = '1.0.0';
		$charset_collate = $wpdb->get_charset_collate();

		if ( $wpdb->get_var( "SHOW TABLES LIKE '{$table_name}'" ) != $table_name ) {

			$sql = "CREATE TABLE `$table_name` (
		  `rating_id` int(10) NOT NULL AUTO_INCREMENT,
		  `rating` int(2) NOT NULL DEFAULT '0',
		  `user_email` varchar(50) NOT NULL,
		  `user_name` varchar(50) NOT NULL,
		  `page_id` int(11) NOT NULL,
		  `Page_url` varchar(50) NOT NULL,
		  `ipaddress` varchar(50) NOT NULL,
		  `dateandtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  PRIMARY KEY (rating_id),
		  UNIQUE KEY (`page_id`,`ipaddress`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
			
	}
	starratingRun();

?>
